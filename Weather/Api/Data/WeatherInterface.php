<?php

namespace Test\Weather\Api\Data;

use Magento\Framework\Exception\NoSuchEntityException;

interface WeatherInterface
{

    const WEATHER = 'weather';

    public function getWeather();

    public function setWeather($weather);
}
