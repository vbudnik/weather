<?php

namespace Test\Weather\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

interface WeatherSearchResultsInterface extends SearchResultsInterface
{

    public function getItems();

    public function setItems(array $items);
}
