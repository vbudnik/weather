<?php
declare(strict_types=1);

namespace Test\Weather\Api;

interface WeatherApiServiceInterface
{

    public function getWeatherApi();
}
