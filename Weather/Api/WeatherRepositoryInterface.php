<?php

namespace Test\Weather\Api;

interface WeatherRepositoryInterface
{

    /**
     * @param Data\WeatherInterface $weather
     * @return mixed
     */
    public function save(Data\WeatherInterface $weather);

    /**
     * @param $id
     * @return mixed
     */
    public function getById($id);

    /**
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return mixed
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);

    /**
     * @param Data\WeatherInterface $weather
     * @return mixed
     */
    public function delete(Data\WeatherInterface $weather);

    /**
     * @param $id
     * @return mixed
     */
    public function deleteById($id);
}
