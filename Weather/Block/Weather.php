<?php

namespace Test\Weather\Block;

use Magento\Framework\View\Element\Template;
use Magento\Framework\Serialize\Serializer\Json;

class Weather extends \Magento\Framework\View\Element\Template
{
    private $jsonHelper;

    public function __construct(
        Template\Context $context,
        Json $jsonHelper,
        array $data = []
    ) {
        $this->jsonHelper = $jsonHelper;
        parent::__construct($context, $data);
    }

    public function getJsLayout()
    {
        return $this->jsonHelper->serialize($this->jsLayout);
    }
}
