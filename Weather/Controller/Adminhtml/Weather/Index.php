<?php
namespace Test\Weather\Controller\Adminhtml\Weather;

class Index extends \Magento\Backend\App\Action
{

    const ADMIN_RESOURCE = 'Test_Weather::list_weather';

    private $_pageFactory;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $pageFactory
    ) {
        parent::__construct($context);
        $this->_pageFactory = $pageFactory;
    }

    public function execute()
    {
        $resultPage = $this->_pageFactory->create();

        $resultPage->getConfig()->getTitle()->prepend(__('Weather Lists'));
        return $resultPage;
    }
}
