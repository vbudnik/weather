<?php

namespace Test\Weather\Controller\Weather;

use Magento\Framework\App\Action\Context;

use Magento\Framework\Controller\ResultFactory;
use Test\Weather\Model\WeatherFactory;
use Test\Weather\Helper\Data as DataHelper;
use Test\Weather\Logger\Logger;

class Index extends \Magento\Framework\App\Action\Action
{
    private $context;
    private $weatherFactory;
    private $dataHelper;
    private $jsonHelper;

    public function __construct(
        Context $context,
        WeatherFactory $weatherFactory,
        DataHelper $dataHelper,
        \Magento\Framework\Serialize\Serializer\Json $jsonHelper
    ) {
        parent::__construct($context);
        $this->context = $context;
        $this->weatherFactory = $weatherFactory;
        $this->dataHelper = $dataHelper;
        $this->jsonHelper = $jsonHelper;
    }

    public function execute()
    {
        $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        try {
            $weather = $this->weatherFactory->create()->getCollection()->getLastItem()->getWeather();
            $resultJson->setHttpResponseCode(\Magento\Framework\Webapi\Response::HTTP_OK);
            $resultJson->setData($weather);
            return $resultJson;
        } catch (\Exception $e) {
            $message = sprintf(
                'Error: %s',
                $e->getMessage()
            );
            $this->dataHelper->log(sprintf(
                '%s %s',
                str_repeat('=', 5),
                $message
            ), [], Logger::ERROR);
        }
        $resultJson->setHttpResponseCode(\Magento\Framework\Webapi\Exception::HTTP_BAD_REQUEST);
        return $resultJson->setData([]);
    }
}
