<?php

namespace Test\Weather\Helper;

use Magento\Framework\App\Helper\Context;
use Test\Weather\Logger\FileHandlerFactory;
use Test\Weather\Logger\Logger;
use Test\Weather\Logger\LoggerFactory;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    const LOGGER_FILE_PATH = '/var/log/weather.log';

    private $logger;

    public function __construct(
        Context $context,
        LoggerFactory $loggerFactory,
        FileHandlerFactory $fileHandlerFactory
    ) {
        $this->logger = $loggerFactory->create();
        $fileHandler = $fileHandlerFactory->create([
            'filename' => self::LOGGER_FILE_PATH
        ]);
        $this->logger->pushHandler($fileHandler);
        parent::__construct($context);
    }

    public function log($message, array $context = [], $level = Logger::DEBUG)
    {
        $this->logger->addRecord($level, $message, $context);
    }
}
