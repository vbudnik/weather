<?php

namespace Test\Weather\Logger;

use Magento\Framework\Logger\Handler\Base;
use Magento\Framework\Filesystem\DriverInterface;

class FileHandler extends Base
{
    public function __construct(
        DriverInterface $filesystem,
        $filename
    ) {
        $this->fileName = $filename;
        parent::__construct($filesystem);
    }
}
