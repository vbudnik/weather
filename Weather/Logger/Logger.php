<?php

namespace Test\Weather\Logger;

class Logger extends \Monolog\Logger
{
    public function __construct(
        array $handlers = [],
        array $processors = []
    ) {
        parent::__construct('weather', $handlers, $processors);
    }
}
