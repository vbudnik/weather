<?php
namespace Test\Weather\Model\ResourceModel;

class Weather extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    public function _construct()
    {
        $this->_init('weather', 'id');
    }
}
