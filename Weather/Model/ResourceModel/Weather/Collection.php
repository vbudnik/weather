<?php

namespace Test\Weather\Model\ResourceModel\Weather;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    public function _construct()
    {
        $this->_init(\Test\Weather\Model\Weather::class, \Test\Weather\Model\ResourceModel\Weather::class);
    }
}
