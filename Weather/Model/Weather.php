<?php
declare(strict_types=1);

namespace Test\Weather\Model;

use Test\Weather\Api\Data\WeatherInterface;

class Weather extends \Magento\Framework\Model\AbstractModel implements WeatherInterface
{

    public function _construct()
    {
        $this->_init(\Test\Weather\Model\ResourceModel\Weather::class);
    }

    public function getWeather()
    {
        return $this->getData(self::WEATHER);
    }

    public function setWeather($weather)
    {
        return $this->setData(self::WEATHER, $weather);
    }
}
