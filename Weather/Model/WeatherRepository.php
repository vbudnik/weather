<?php
declare(strict_types=1);

namespace Test\Weather\Model;

use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Test\Weather\Api\Data;
use Test\Weather\Api\WeatherRepositoryInterface;
use Test\Weather\Model\WeatherFactory;
use Test\Weather\Model\ResourceModel\Weather\CollectionFactory;
use Test\Weather\Model\ResourceModel\Weather as ResourceWeather;

class WeatherRepository implements WeatherRepositoryInterface
{

    private $resource;
    private $weatherFactory;
    private $weatherCollectionFactory;
    private $collectionProcessor;
    private $searchResultsFactory;

    public function __construct(
        ResourceWeather $resource,
        WeatherFactory $weatherFactory,
        CollectionFactory $weatherCollectionFactory,
        Data\WeatherSearchResultsInterfaceFactory $searchResult,
        CollectionProcessorInterface $collectionProcessor = null
    ) {
        $this->resource = $resource;
        $this->weatherFactory = $weatherFactory;
        $this->weatherCollectionFactory = $weatherCollectionFactory;
        $this->searchResultsFactory = $searchResult;
        $this->collectionProcessor = $collectionProcessor;
    }

    public function save(Data\WeatherInterface $weather)
    {
        try {
            $this->resource->save($weather);
        } catch (\Exception $e) {
            throw new CouldNotSaveException(__($e->getMessage()));
        }
        return $weather;
    }

    public function getById($id)
    {
        $weather = $this->weatherFactory->create();
        $this->resource->load($weather, $id);
        if (!$weather->getId()) {
            throw new NoSuchEntityException(__('The weather with the "%1" ID doesn\'t exist.', $id));
        }
        return $weather;
    }

    public function getList(SearchCriteriaInterface $criteria)
    {
        $collection = $this->weatherCollectionFactory->create();
        $this->collectionProcessor->process($criteria, $collection);
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        $searchResults->setItems($collection->getItems());
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    public function delete(Data\WeatherInterface $weather)
    {
        try {
            $this->resource->delete($weather);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__($exception->getMessage()));
        }
        return true;
    }

    public function deleteById($id)
    {
        return $this->delete($this->getById($id));
    }
}
