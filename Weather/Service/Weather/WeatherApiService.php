<?php
declare(strict_types=1);

namespace Test\Weather\Service\Weather;

use Test\Weather\Api\WeatherApiServiceInterface;
use Magento\Framework\Serialize\Serializer\Json;
use GuzzleHttp\ClientFactory;
use GuzzleHttp\Exception\GuzzleException;
use Magento\Framework\Webapi\Rest\Request;
use Test\Weather\Helper\Data as DataHelper;
use Test\Weather\Logger\Logger;

class WeatherApiService implements WeatherApiServiceInterface
{

    const SERVICE_URL = 'https://api.openweathermap.org/';
    const API_REQUEST_ENDPOINT = 'data/2.5/weather?q=lublin,pl&units=metric&APPID=f82a3d14921f1edd8de8f1f8d969b012';

    private $jsonSerializer;
    private $clientFactory;
    private $dataHelper;

    public function __construct(
        ClientFactory $clientFactory,
        Json $jsonSerializer,
        DataHelper $dataHelper
    ) {
        $this->dataHelper = $dataHelper;
        $this->clientFactory = $clientFactory;
        $this->jsonSerializer = $jsonSerializer;
    }

    public function getWeatherApi()
    {
        try {
            $response = $this->doRequest(self::SERVICE_URL . self::API_REQUEST_ENDPOINT, [], Request::HTTP_METHOD_GET);
            $status = $response->getStatusCode();
            if ($status == 200) {
                $content = $responseBody = $response->getBody()->getContents();

                $message = sprintf(
                    'API request by value: %s',
                    $content
                );
                $this->dataHelper->log(sprintf(
                    '%s %s',
                    str_repeat('=', 5),
                    $message
                ), [], Logger::INFO);

                return $this->jsonSerializer->unserialize($content);
            }
        } catch (GuzzleException | \Exception $e) {
            $message = sprintf(
                'Error during get API request: %s',
                $e->getMessage()
            );
            $this->dataHelper->log(sprintf(
                '%s %s',
                str_repeat('=', 5),
                $message
            ), [], Logger::ERROR);
            return [];
        }
        return [];
    }

    private function doRequest($uriEndpoint, $params = [], $requestMethod = Request::HTTP_METHOD_GET)
    {
        $client = $this->clientFactory->create(['config' => [
            'base_uri' => self::SERVICE_URL
        ]]);
        try {
            $response = $client->request(
                $requestMethod,
                $uriEndpoint,
                $params
            );
        } catch (GuzzleException $exception) {
            $response = $this->responseFactory->create([
                'status' => $exception->getCode(),
                'reason' => $exception->getMessage()
            ]);

            $message = sprintf(
                'Error during get API request: %s',
                $response
            );
            $this->dataHelper->log(sprintf(
                '%s %s',
                str_repeat('=', 5),
                $message
            ), [], Logger::ERROR);
        }

        return $response;
    }
}
