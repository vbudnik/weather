<?php

declare(strict_types=1);

namespace Test\Weather\Setup\Patch\Data;

use Magento\Framework\Setup\Patch\DataPatchInterface;

class AddWeather implements DataPatchInterface
{

    private $weatherService;
    private $weatherRepository;
    private $weatherFactory;
    private $dataHelper;
    private $jsonHelper;

    public function __construct(
        \Test\Weather\Service\Weather\WeatherApiService $weatherService,
        \Test\Weather\Model\WeatherFactory $weatherFactory,
        \Test\Weather\Api\WeatherRepositoryInterface $weatherRepository,
        \Test\Weather\Helper\Data $dataHelper,
        \Magento\Framework\Serialize\Serializer\Json $jsonHelper
    ) {
        $this->weatherService = $weatherService;
        $this->weatherFactory = $weatherFactory;
        $this->weatherRepository = $weatherRepository;
        $this->dataHelper = $dataHelper;
        $this->jsonHelper = $jsonHelper;
    }
    public function apply()
    {
        try {
            $response = $this->weatherService->getWeatherApi();

            $weather = $this->weatherFactory->create();
            $weather->setData([
                'weather' => $this->jsonHelper->serialize($response)
            ]);

            $this->weatherRepository->save($weather);

        } catch (\Exception $e) {
            $this->dataHelper->log($e->getMessage());
        }

    }

    /**
     * @return array|string[]
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * @return array|string[]
     */
    public function getAliases()
    {
        return [];
    }
}
