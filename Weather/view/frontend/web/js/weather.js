define([
    'jquery',
    'uiComponent',
    'mage/url'
], function($, Component, url) {
    'use strict';

    return Component.extend({

        getAjaxWeather: (function ()  {
            getWeather();
            setInterval(function() {
                getWeather();
            }, 600000);
            function getWeather() {
                $.get(url.build('weather/weather'), function (data) {
                    if (!data) {
                        return ;
                    }
                    let weather = JSON.parse(data);
                    let temp = weather.main ? weather.main : null;
                    $('.weather-temp #value').text(temp.temp ? parseInt(temp.temp ) : "-");
                    $('.weather-feels_like #value').text(temp.feels_like ? parseInt(temp.feels_like) : "-");
                });
            }
        })(),

    });
});
